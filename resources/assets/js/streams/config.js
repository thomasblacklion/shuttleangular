(function () {

    'use strict';

    angular.module('streams', []);

    angular
        .module('streams')
        .config(function ($stateProvider) {

            $stateProvider
                .state('streams', {
                    url: '/streams',
                    templateUrl: "templates/streams/overview.html",
                })

                .state('streams.detail', {
                    url: '/streams/:id',
                    templateUrl: "templates/streams/detail.html"
                });

        });
})();
