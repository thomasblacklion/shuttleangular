(function () {
    angular.module('app', [

        'ui.router',                    // Routing
        'ngResource',
        //'as.sortable',
        'selectize',

        'system',
        'sites',
        'streams'
    ])
})();
