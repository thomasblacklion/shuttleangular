(function () {
    'use strict';

    angular.module('system', []);

    angular
        .module('system')
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {

            $urlRouterProvider.otherwise("/start");

            //make sure laravel request->ajax() still works
            $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

            //add a hard reload when csrf token mismatches
            //$httpProvider.interceptors.push('csrfHandler');
            //$httpProvider.interceptors.push('authHandler');

            //$locationProvider.html5Mode(true);

            $stateProvider

                .state('start', {
                    url: "/start",
                    templateUrl: "templates/start.html",
                })

        })
        .run(function ($rootScope, $state) {
            $rootScope.$state = $state;

            //$rootScope.$on('$stateChangeStart',
            //    function (event, toState, toParams, fromState, fromParams) {
            //
            //        //make sure users need to select their locale.
            //        System.then(function () {
            //            if (!System.user.locale_id && toState.name != 'admin.profile') {
            //                $state.go('admin.profile');
            //                event.preventDefault();
            //            }
            //
            //            if(System.user.locale_id)
            //            {
            //                moment.locale(System.user.locale.slug);
            //            }
            //        });
            //
            //    })
        });


})();
