(function() {

    'use strict';

    angular.module('sites')
        .factory('SitesService', function(Site){

            function SiteService()
            {
                return {

                    load: function()
                    {
                        return Site.get().$promise;
                    }


                }

            }


            return new SiteService()
        });

})();
