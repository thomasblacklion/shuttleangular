(function(){

    'use strict';


    angular.module('sites')

        .factory('SiteResource', function($resource)
        {
            return $resource('/admin/api/sites/:id', {
                id: '@id',
            }, {
                update:{
                    method : 'PUT'
                }
            });
        })

        .factory('Site', function($http, SiteResource){

            var site = {

                activatePackage: function(params)
                {
                    return $http({
                        url: '/admin/api/sites/activate-package',
                        method: 'POST',
                        params: params,
                        transformResponse: function(response){
                            response = angular.fromJson(response);
                            return new SiteResource(response);
                        }
                    });
                },
                deActivatePackage: function()
                {
                    return $http({
                        url: '/admin/api/sites/deactivate-package',
                        method: 'POST',
                        params: params,
                        transformResponse: function(response){
                            response = angular.fromJson(response);
                            return new SiteResource(response);
                        }
                    });
                }

            };

            return angular.extend(site, SiteResource);

        });

})();
