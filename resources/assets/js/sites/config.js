(function () {

    'use strict';

    angular.module('sites', []);

    angular.module('sites')
        .config(function ($stateProvider) {

        $stateProvider
            .state('sites', {
                url: '/sites',

                templateUrl: "templates/sites/overview.html"
            })

            .state('sites.detail', {
                url: '/sites/:id',
                templateUrl: "templates/sites/detail.html"
            })

    });

})();
