(function() {

    'use strict';

    angular.module('sites')
        .controller('SitesOverviewController', SitesOverviewController);

    function SitesOverviewController(SitesService)
    {
        this.sites = [];
        this.load = load;

        var vm = this;

        this.load();

        function load()
        {
            SitesService.load().then(function(sites){
                vm.sites = sites;
            });
        }

    }

})();
