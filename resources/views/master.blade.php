<!DOCTYPE html>
<html ng-app="app">
    <head>
        <title>Laravel</title>

        <base href="/"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

    </head>
    <body>

        <div ng-include="'templates/navigation.html'"></div>

        <div id="wrapper">

            <div id="page-wrapper" class="container @{{$state.current.name}}">

                <!-- Main view  -->
                <div ui-view></div>

            </div>
            <!-- End page wrapper-->

        </div>

    </body>

    <script type="text/javascript" src="/js/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/js/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/scripts/lodash.min.js"></script>
    <script type="text/javascript" src="/js/scripts/selectize.min.js"></script>
    <script type="text/javascript" src="/js/scripts/angular.min.js"></script>
    <script type="text/javascript" src="/js/scripts/angular-resource.min.js"></script>
    <script type="text/javascript" src="/js/scripts/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="/js/scripts/angular-selectize.js"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</html>
