var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss');


    var files = {
        'bower_components/jquery/dist/jquery.min.js': 'public/js/scripts/',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js': 'public/js/scripts/',
        'bower_components/lodash/dist/lodash.min.js': 'public/js/scripts/',
        'bower_components/selectize/dist/js/standalone/selectize.min.js': 'public/js/scripts/',
        'bower_components/angular/angular.min.js': 'public/js/scripts/',
        'bower_components/angular-resource/angular-resource.min.js': 'public/js/scripts/',
        'bower_components/angular-ui-router/release/angular-ui-router.min.js': 'public/js/scripts/',
        'bower_components/angular-selectize2/dist/angular-selectize.js': 'public/js/scripts/'
    };

    for (file in files) {
        mix.copy(file, files[file]);
    }

    mix.scripts([
        'app.js',
        'config.js',
        '**/config.js',
        '**/models/*.js',
        '**/services/*.js',
        '**/controllers/*.js',
        '**/directives/*.js'
    ], 'public/js/app.js');
});
