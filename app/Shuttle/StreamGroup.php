<?php namespace App\Shuttle;

use Illuminate\Database\Eloquent\Model;

class StreamGroup extends Model
{

    protected $table = 'core_stream_group';

    protected $fillable = [
        'name',
        'order',
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'order' => 'integer',
    ];

    public function streams()
    {
        return $this->hasMany(Stream::class, 'group_id');
    }

}
