<?php namespace App\Shuttle;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{

    protected $table = 'core_stream';

    protected $fillable = [
        'stream_name',
        'stream_namespace',
        'is_translatable',
        'relation_display',
        'hidden',
        'order',
        'show_id',
        'is_pivot_table',
        'sort_order_field_id',
        'sort_order_direction',
    ];


    protected $casts = [
        'id' => 'integer',
        'group_id' => 'integer',
        'stream_name' => 'string',
        'stream_slug' => 'string',
        'stream_slug_language' => 'string',
        'stream_namespace' => 'string',
        'is_translatable' => 'boolean',
        'relation_display' => 'string',
        'system' => 'boolean',
        'hidden' => 'boolean',
        'order' => 'integer',
        'show_id' => 'boolean',
        'is_pivot_table' => 'boolean',
        'sort_order_field_id' => 'integer',
        'sort_order_direction' => 'string',
    ];


    public function group()
    {
        return $this->belongsTo(StreamGroup::class, 'group_id');
    }

    public function fields()
    {
        return $this->hasMany(StreamField::class, 'stream_id');
    }

    public function sortOrderField()
    {
        return $this->belongsTo(StreamField::class, 'sort_order_field_id');
    }

}
