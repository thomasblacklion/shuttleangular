<?php namespace App\Shuttle;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{

    protected $table = 'global_site';

    protected $fillable = ['name', 'database', 'bucket', 'packages', 'slug_main', 'ssl', 'thumbnail_timestamp'];

    protected $casts = [
        'id' => 'integer',
        'company_id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'database' => 'string',
        'bucket' => 'string',
        'packages' => 'array',
        'slug_main' => 'boolean',
        'ssl' => 'boolean',
        'thumbnail_timestamp' => 'datetime',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
