<?php namespace App\Shuttle;

use Illuminate\Database\Eloquent\Model;

class StreamField extends Model
{

    protected $table = 'core_stream_field';

    protected $fillable = [
        'field_name',
        'field_properties',
        'order',
        'is_required',
        'is_translatable',
        'is_unique',
        'is_title_field',
        'is_data_column',
        'validation_rules',
        'hidden',
        'group_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'stream_id' => 'integer',
        'field_name' => 'string',
        'field_slug' => 'string',
        'field_type' => 'string',
        'field_properties' => 'array',
        'order' => 'integer',
        'is_required' => 'boolean',
        'is_translatable' => 'boolean',
        'is_unique' => 'boolean',
        'is_title_field' => 'boolean',
        'is_data_column' => 'boolean',
        'validation_rules' => 'string',
        'system' => 'boolean',
        'hidden' => 'boolean',
        'group_id' => 'integer',
    ];

    public function stream()
    {
        return $this->belongsTo(Stream::class, 'stream_id');
    }

    public function fieldGroup()
    {
        return $this->belongsTo(StreamField::class, 'group_id');
    }

}
