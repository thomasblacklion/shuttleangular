<?php namespace App\Shuttle;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table = 'global_company';

    protected $fillable = ['name'];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
    ];

    public function sites()
    {
        return $this->hasMany(Site::class, 'company_id');
    }

}
