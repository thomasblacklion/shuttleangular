<?php namespace App\Http\Controllers;

use App\Shuttle\Site;
use Illuminate\Http\Request;

class SitesController extends Controller
{

    public function index(Site $site, Request $request)
    {
        $builder = $site->newQuery();

        $query = $request->get('query', false);

        if($query)
        {
            $builder->where('name', 'like', "%$query%");
        }

        $company = $request->get('company_id');

        if($company)
        {
            $builder->where('company_id', $company);
        }

        return $builder
            ->paginate(9);
    }

    public function show(Site $site)
    {
        return $site;
    }

    public function store(Request $request)
    {
        $site = new Site($request->all());
        $site->save();

        return $site;
    }

    public function update(Site $site, Request $request)
    {
        $site->fill($request->all());
        $site->save();

        return $site;
    }

    public function destroy(Site $site)
    {
        $site->delete();
    }

}
