<?php namespace App\Http\Controllers;

use App\Shuttle\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function index(Company $site)
    {
        return $site->all();
    }

    public function show(Company $site)
    {
        return $site;
    }

    public function store(Request $request)
    {
        $site = new Company($request->all());
        $site->save();

        return $site;
    }

    public function update(Company $site, Request $request)
    {
        $site->fill($request->all());
        $site->save();

        return $site;
    }

    public function destroy(Company $site)
    {
        $site->delete();
    }
}
