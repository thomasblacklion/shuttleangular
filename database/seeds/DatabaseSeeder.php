<?php

use App\Shuttle\Company;
use App\Shuttle\Site;
use App\Shuttle\Stream;
use App\Shuttle\StreamField;
use App\Shuttle\StreamGroup;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCompaniesAndSites();
        $this->seedStreams();
    }

    protected function seedCompaniesAndSites()
    {
        $companies = factory(Company::class)->times(25)->create();

        factory(Site::class)->times(100)->create()->each(function ($site) use ($companies) {
            if(rand(0,1))
            {
                $site->company()->associate($companies->random(1));
                $site->save();
            }
        });
    }

    protected function seedStreams()
    {
        $groups = factory(StreamGroup::class)->times(5)->create();
        $streams = factory(Stream::class)->times(15)->create();

        $streams->each(function ($stream) use ($groups) {

            if (rand(0, 1)) {
                $stream->group()->associate($groups->random(1));
                $stream->save();
            }

            if(rand(0,1))
            {
                $amount = rand(6, 9);
            }
            else{
                $amount = rand(3, 5);
            }


            $columns = [
                'name',
                'slug',
                'hidden',
                'email',
                'website',
                'product_id',
                'product_reference',
                'promotion_id',
                'firstname',
                'lastname',
                'title',
                'content'
            ];

            $columns = array_rand($columns, $amount);

            foreach ($columns as $column) {
                $field = factory(StreamField::class)->make([
                    'field_name' => $column,
                ]);

                $stream->fields()->save($field);
            }
        });
    }
}
