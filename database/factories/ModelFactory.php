<?php

use App\Shuttle\Site;
use App\Shuttle\Company;
use App\Shuttle\Stream;
use App\Shuttle\StreamGroup;
use App\Shuttle\StreamField;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'created_at' => $faker->dateTimeBetween('-10 months', '-5 months'),
        'updated_at' => $faker->dateTimeBetween('-4 months', 'now'),
    ];
});


$factory->define(Site::class, function (Faker\Generator $faker) {

    $company_id = rand(0,1) ? Company::all()->random(1)->id : null;

    $name = $faker->unique()->company;
    $slug = str_slug($name);

    return [
        'company_id' => $company_id,
        'name' => $name,
        'slug' => $slug,
        'database' => $slug,
        'bucket' => $slug,
        'slug_main' => $faker->boolean(10),
        'ssl' => $faker->boolean(10),
        'thumbnail_timestamp' => $faker->dateTime,
        'created_at' => $faker->dateTimeBetween('-10 months', '-5 months'),
        'updated_at' => $faker->dateTimeBetween('-4 months', 'now'),
    ];
});

$factory->define(StreamGroup::class, function (Faker\Generator $faker) {
    return [
        'name' => 'group_' . $faker->unique()->colorName,
        'created_at' => $faker->dateTimeBetween('-10 months', '-5 months'),
        'updated_at' => $faker->dateTimeBetween('-4 months', 'now'),
    ];
});

$factory->define(Stream::class, function (Faker\Generator $faker) {

    $name = 'stream_' . $faker->unique()->colorName;
    $slug = str_slug($name);
    return [
        'stream_name' => $name,
        'stream_slug' => $slug,
        'stream_slug_language' => $slug . '_language',
        'is_translatable' => $faker->boolean(),
        'system' => $faker->boolean(),
        'hidden' => $faker->boolean(),
        'show_id' => $faker->boolean(),
        'is_pivot_table' => $faker->boolean(),
        'sort_order_direction' => rand(0,1) ? 'asc' : 'desc',
        'created_at' => $faker->dateTimeBetween('-10 months', '-5 months'),
        'updated_at' => $faker->dateTimeBetween('-4 months', 'now'),
    ];
});


$factory->define(StreamField::class, function (Faker\Generator $faker) {

    $types = ['text', 'textarea', 'relation', 'numeric', 'price', 'datetime', 'asset', 'password', 'boolean'];

    $name = 'field_' . $faker->unique()->colorName;
    $slug = str_slug($name);
    return [
        'field_name' => $name,
        'field_slug' => $slug,
        'field_type' => $types[array_rand($types, 1)],
        'is_required' => $faker->boolean,
        'is_translatable' => $faker->boolean,
        'is_unique' => $faker->boolean,
        'is_title_field' => $faker->boolean,
        'is_data_column' => $faker->boolean,
        'system' => $faker->boolean,
        'hidden' => $faker->boolean,
        'created_at' => $faker->dateTimeBetween('-10 months', '-5 months'),
        'updated_at' => $faker->dateTimeBetween('-4 months', 'now'),
    ];
});
