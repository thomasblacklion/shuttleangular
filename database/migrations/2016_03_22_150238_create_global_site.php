<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalSite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_site', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id', false, true)->nullable();
            $table->foreign('company_id', 'site_to_company')->references('id')->on('global_company')->onDelete('cascade');

            $table->string('name', 50);
            $table->string('slug', 50);
            $table->string('database', 50);
            $table->string('bucket', 50);
            $table->string('packages', 50);
            $table->boolean('slug_main');
            $table->boolean('ssl');
            $table->dateTime('thumbnail_timestamp');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('global_site');
    }
}
