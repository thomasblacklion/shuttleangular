<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreStream extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_stream_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('order', false, true);
            $table->timestamps();
        });

        Schema::create('core_stream', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('group_id', false, true)->nullable();
            $table->foreign('group_id', 'stream_to_stream_group')->references('id')->on('core_stream_group')->onDelete('set null');

            $table->string('stream_name', 64);
            $table->string('stream_slug', 64);
            $table->string('stream_slug_language', 64);
            $table->string('stream_namespace', 50);
            $table->boolean('is_translatable');
            $table->string('relation_display');
            $table->boolean('system');
            $table->boolean('hidden');
            $table->integer('order', false, true);
            $table->boolean('show_id');
            $table->boolean('is_pivot_table');
            $table->integer('sort_order_field_id', false, true)->nullable();
            $table->string('sort_order_direction', 5);

            $table->timestamps();
        });

        Schema::create('core_stream_field', function(Blueprint $table){
            $table->increments('id');
            $table->integer('stream_id', false, true);
            $table->foreign('stream_id', 'field_to_stream')->references('id')->on('core_stream')->onDelete('cascade');

            $table->string('field_name', 50);
            $table->string('field_slug', 50);
            $table->string('field_type', 50);
            $table->text('field_properties');

            $table->integer('order', false, true);
            $table->boolean('is_required');
            $table->boolean('is_translatable');
            $table->boolean('is_unique');
            $table->boolean('is_title_field');
            $table->boolean('is_data_column');

            $table->string('validation_rules');
            $table->boolean('system');
            $table->boolean('hidden');

            $table->timestamps();

        });

        Schema::table('core_stream_field', function(Blueprint $table){
            $table->integer('group_id', false, true)->nullable()->after('hidden');
            $table->foreign('group_id', 'grouped_field_to_parent_field')->references('id')->on('core_stream_field')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('core_stream_field', function(Blueprint $table){
            $table->dropForeign('grouped_field_to_parent_field');
            $table->dropColumn('group_id');
        });
        Schema::drop('core_stream_field');
        Schema::drop('core_stream');
        Schema::drop('core_stream_group');
    }
}
