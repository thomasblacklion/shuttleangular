(function () {
    angular.module('app', [

        'ui.router',                    // Routing
        'ngResource',
        //'as.sortable',
        'selectize',

        'system',
        'sites',
        'streams'
    ])
})();

(function () {
    'use strict';

    angular.module('system', []);

    angular
        .module('system')
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {

            $urlRouterProvider.otherwise("/start");

            //make sure laravel request->ajax() still works
            $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

            //add a hard reload when csrf token mismatches
            //$httpProvider.interceptors.push('csrfHandler');
            //$httpProvider.interceptors.push('authHandler');

            //$locationProvider.html5Mode(true);

            $stateProvider

                .state('start', {
                    url: "/start",
                    templateUrl: "templates/start.html",
                })

        })
        .run(function ($rootScope, $state) {
            $rootScope.$state = $state;

            //$rootScope.$on('$stateChangeStart',
            //    function (event, toState, toParams, fromState, fromParams) {
            //
            //        //make sure users need to select their locale.
            //        System.then(function () {
            //            if (!System.user.locale_id && toState.name != 'admin.profile') {
            //                $state.go('admin.profile');
            //                event.preventDefault();
            //            }
            //
            //            if(System.user.locale_id)
            //            {
            //                moment.locale(System.user.locale.slug);
            //            }
            //        });
            //
            //    })
        });


})();

(function () {

    'use strict';

    angular.module('sites', []);

    angular.module('sites')
        .config(function ($stateProvider) {

        $stateProvider
            .state('sites', {
                url: '/sites',

                templateUrl: "templates/sites/overview.html"
            })

            .state('sites.detail', {
                url: '/sites/:id',
                templateUrl: "templates/sites/detail.html"
            })

    });

})();

(function () {

    'use strict';

    angular.module('streams', []);

    angular
        .module('streams')
        .config(function ($stateProvider) {

            $stateProvider
                .state('streams', {
                    url: '/streams',
                    templateUrl: "templates/streams/overview.html",
                })

                .state('streams.detail', {
                    url: '/streams/:id',
                    templateUrl: "templates/streams/detail.html"
                });

        });
})();

(function(){

    'use strict';


    angular.module('sites')

        .factory('SiteResource', function($resource)
        {
            return $resource('/admin/api/sites/:id', {
                id: '@id',
            }, {
                update:{
                    method : 'PUT'
                }
            });
        })

        .factory('Site', function($http, SiteResource){

            var site = {

                activatePackage: function(params)
                {
                    return $http({
                        url: '/admin/api/sites/activate-package',
                        method: 'POST',
                        params: params,
                        transformResponse: function(response){
                            response = angular.fromJson(response);
                            return new SiteResource(response);
                        }
                    });
                },
                deActivatePackage: function()
                {
                    return $http({
                        url: '/admin/api/sites/deactivate-package',
                        method: 'POST',
                        params: params,
                        transformResponse: function(response){
                            response = angular.fromJson(response);
                            return new SiteResource(response);
                        }
                    });
                }

            };

            return angular.extend(site, SiteResource);

        });

})();

(function() {

    'use strict';

    angular.module('sites')
        .factory('SitesService', function(Site){

            function SiteService()
            {
                return {

                    load: function()
                    {
                        return Site.get().$promise;
                    }


                }

            }


            return new SiteService()
        });

})();

(function() {

    'use strict';

    angular.module('sites')
        .controller('SitesOverviewController', SitesOverviewController);

    function SitesOverviewController(SitesService)
    {
        this.sites = [];
        this.load = load;

        var vm = this;

        this.load();

        function load()
        {
            SitesService.load().then(function(sites){
                vm.sites = sites;
            });
        }

    }

})();

//# sourceMappingURL=app.js.map
